from django.contrib import admin
from rooms.models import Room, RoomType, Equipment
from hotelmanager.models import UserProfile, Hotel

admin.site.register(Room)
admin.site.register(RoomType)
admin.site.register(Equipment)
admin.site.register(UserProfile)
admin.site.register(Hotel)
