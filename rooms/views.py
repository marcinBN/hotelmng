from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic

from rooms.models import Room, RoomForm

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from django.views.generic import View

from django.views.generic.edit import FormView


class IndexView(generic.ListView):
    template_name = 'rooms/index.html'
    context_object_name = 'all_rooms_list'

    #@method_decorator(login_required)
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['room_form'] = RoomForm
        return context

    #@method_decorator(login_required)
    def get_queryset(self):
        return Room.objects.all()


class DetailView(generic.DetailView):
    model = Room
    slug_field = 'number'
    template_name = 'rooms/detail.html'

# not used, but works fine
def add(request):
    print 'test'
    errors = []
    if request.method == 'POST':
        if not request.POST.get('number', ''):
            errors.append('Enter a number of the room.')
        if not errors:
            new = Room(number=request.POST['number'],
                       available=request.POST['available'],
                       prepared=request.POST['prepared'],
                       condignation=request.POST['condignation'])
            new.save()
            return HttpResponseRedirect('/rooms/')
    return render(request, 'rooms/index.html', {'errors': errors})

def add_room(request):
    print "entered add_room function"
    if request.method == 'POST':
        form = RoomForm(request.POST, request.FILES)
        if form.is_valid():
            print "form is valid"
            form.save()
            return HttpResponseRedirect('/rooms/')
        else:
            print "form is NOT valid: ", form.errors
            form = RoomForm()

    return render(request, 'rooms/index.html', {
        'room_form': form,
    })


class AddRoomView(View):
    form_class = RoomForm
    initial = {'key': 'value'}
    template_name = 'rooms/index.html'

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            # <process form cleaned data>
            form.save()
            return HttpResponseRedirect('/rooms/')

        return render(request, self.template_name, {'room_form': form})

class AddRoomVieww(FormView):
    template_name = 'rooms/index.html'
    form_class = RoomForm
    success_url = '/rooms/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        #form.send_email()
        return super(AddRoomView, self).form_valid(form)



