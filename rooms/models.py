from django.db import models
from django.forms import ModelForm

#from django.contrib.auth.models import User

from hotelmanager.models import Hotel


class RoomType(models.Model):
    name = models.CharField(max_length=100, unique=True)
    beds = models.IntegerField(null=True)


    def __unicode__(self):
        return u"%s with %s beds" % (self.name, self.beds)


class Equipment(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name



class Room(models.Model):
    hotel = models.ForeignKey(Hotel, default=1)
    roomtype = models.ForeignKey(RoomType, default=1)
    number = models.CharField(max_length=100, unique=True)
    available = models.BooleanField(default=True)
    prepared = models.BooleanField(default=True)
    windowview = models.CharField(max_length=100, null=True)
    condignation = models.IntegerField(null=True)
    cost = models.IntegerField(null=True)
    equipments = models.ManyToManyField(Equipment, null=True)
    img = models.ImageField(null=True, upload_to='room_type_img')

    def __unicode__(self):
        return self.number


class RoomForm(ModelForm):
    class Meta:
        model = Room


class RoomTypeForm(ModelForm):
    class Meta:
        model = RoomType



