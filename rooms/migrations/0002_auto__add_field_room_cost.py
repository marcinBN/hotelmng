# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Room.cost'
        db.add_column(u'rooms_room', 'cost',
                      self.gf('django.db.models.fields.IntegerField')(null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Room.cost'
        db.delete_column(u'rooms_room', 'cost')


    models = {
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'condignation': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'equipment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'prepared': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'windowview': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['rooms']