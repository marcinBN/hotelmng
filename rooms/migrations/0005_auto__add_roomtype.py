# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RoomType'
        db.create_table(u'rooms_roomtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('beds', self.gf('django.db.models.fields.IntegerField')(null=True)),
        ))
        db.send_create_signal(u'rooms', ['RoomType'])


    def backwards(self, orm):
        # Deleting model 'RoomType'
        db.delete_table(u'rooms_roomtype')


    models = {
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'condignation': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'equipment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'prepared': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'windowview': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        u'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'beds': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        }
    }

    complete_apps = ['rooms']