# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding M2M table for field equipments on 'Room'
        db.create_table(u'rooms_room_equipments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('room', models.ForeignKey(orm[u'rooms.room'], null=False)),
            ('equipment', models.ForeignKey(orm[u'rooms.equipment'], null=False))
        ))
        db.create_unique(u'rooms_room_equipments', ['room_id', 'equipment_id'])


    def backwards(self, orm):
        # Removing M2M table for field equipments on 'Room'
        db.delete_table('rooms_room_equipments')


    models = {
        u'rooms.equipment': {
            'Meta': {'object_name': 'Equipment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'condignation': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'equipments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['rooms.Equipment']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'prepared': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'roomtype': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['rooms.RoomType']"}),
            'windowview': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        u'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'beds': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        }
    }

    complete_apps = ['rooms']