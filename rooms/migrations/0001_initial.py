# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Room'
        db.create_table(u'rooms_room', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('number', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('prepared', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('windowview', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
            ('condignation', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('equipment', self.gf('django.db.models.fields.CharField')(max_length=100, null=True)),
        ))
        db.send_create_signal(u'rooms', ['Room'])


    def backwards(self, orm):
        # Deleting model 'Room'
        db.delete_table(u'rooms_room')


    models = {
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'condignation': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'equipment': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'prepared': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'windowview': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        }
    }

    complete_apps = ['rooms']