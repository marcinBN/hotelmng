# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'RoomType.img'
        db.delete_column(u'rooms_roomtype', 'img')

        # Adding field 'Room.img'
        db.add_column(u'rooms_room', 'img',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'RoomType.img'
        db.add_column(u'rooms_roomtype', 'img',
                      self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True),
                      keep_default=False)

        # Deleting field 'Room.img'
        db.delete_column(u'rooms_room', 'img')


    models = {
        u'rooms.equipment': {
            'Meta': {'object_name': 'Equipment'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'rooms.room': {
            'Meta': {'object_name': 'Room'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'condignation': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'equipments': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['rooms.Equipment']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'prepared': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'roomtype': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['rooms.RoomType']"}),
            'windowview': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'})
        },
        u'rooms.roomtype': {
            'Meta': {'object_name': 'RoomType'},
            'beds': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        }
    }

    complete_apps = ['rooms']