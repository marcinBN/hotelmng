from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static
from rooms import views

from django.contrib.auth.decorators import login_required, permission_required


urlpatterns = patterns('',
        url(r'^$', login_required(views.IndexView.as_view()), name='index'),
        url(r'^add/$', login_required(views.add_room), name='add_room'),
        url(r'^(?P<slug>\w+)/$', login_required(views.DetailView.as_view()), name='detail'),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

