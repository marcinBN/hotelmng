from django.conf.urls import patterns, include, url
from hotelmanager import views
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hotelmanager.views.home', name='home'),
    # url(r'^hotelmanager/', include('hotelmanager.foo.urls')),


    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', views.HomeView.as_view(), name='home'),
    url(r'^rooms/', include('rooms.urls', namespace='rooms')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^(?P<slug>.+)/$', views.HotelView.as_view(), name='detail'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
