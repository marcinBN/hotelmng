from django.db import models
from django.utils.text import slugify
from django.contrib.auth.models import User




class Hotel(models.Model):
    name = models.CharField(max_length=100)
    slugp = models.SlugField(max_length=255, unique=True, default=u"generic")

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slugp = slugify(self.name)
        super(Hotel, self).save(*args, **kwargs)


class UserProfile(models.Model):
    hotels = models.ManyToManyField(Hotel)

    user = models.OneToOneField(User)
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='users_img', blank=True)

    def __unicode__(self):
        return self.user.username








