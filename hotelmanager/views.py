from django.views.generic import TemplateView, DetailView
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from hotelmanager.models import Hotel
from .forms import UserForm, UserProfileForm
from rooms.models import Room, RoomForm


class HomeView(TemplateView):
    template_name = "home.html"


class HotelView(DetailView):
    model = Hotel
    slug_field = "slugp"
    template_name = "detail.html"


    def get_context_data(self, **kwargs):
        context = super(HotelView, self).get_context_data(**kwargs)
        context['all_rooms_list'] = Room.objects.filter(hotel__slugp=self.kwargs['slug'])
        context['room_form'] = RoomForm
        return context

def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect('/')
            # Redirect to success page
        else:
            return HttpResponse("Account disabled")
            # 'Disabled account' error message
    else:
        return HttpResponse("Invalid login")
        # 'Invalid login' error message

@login_required
def logout_view(request):
    logout(request)

    return HttpResponseRedirect('/')

def register_view(request):
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            profile = profile_form.save(commit=False)
            profile.user = user

            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']
            profile.save()
            registered = True
        else:
            print user_form.errors, profile_form.errors

    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(request, 'registration.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered}
            )

